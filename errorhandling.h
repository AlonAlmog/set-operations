#define WORDS_IN_THREE_SET_COMMAND 6

bool isInteger(char *line);
bool integerIsInRange(int num);
bool setValid(char *word);
bool inputIsTerminatingValue(char *word);
bool missingParametersInThreeSetCommand(int wordNumberInLine, char *word);
bool extraneousTextFoundInThreeSetCommand(int wordNumberInLine, char *word);
bool extraneousTextFoundInStopCommand(char *word);
bool extraneousTextFoundAfterInputTerminatingValue(char *word);
bool wordIsInNumberPosition(int wordIndexInLine);
bool missingCommaInCommaPosition(int wordIndexInLine, char *word);
bool illegalComma(char *word);
bool wordIsComma(char *word);
bool foundConsecutiveCommas(char *line);
bool inputHasNotEndedWithTerminatingValue(bool inputTerminateValueRecieved, char *word);
bool errorsFoundInStopCommand(char *line);
bool errorsFoundInPrintCommand(char *line);
bool errorsFoundInReadCommand(char *line);
bool errorsFoundInThreeSetCommand(char *line);
void printFailureAndExit();
