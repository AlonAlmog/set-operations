#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>
#include "set.h"

/* File contains functions for each command, union_set, print_set, read_set, etc.
   command operands are passed as function parameters. */

/* Returns true if it's the first iteration and the head of the numbers list has a terminating value. */
bool headIsTerminatingValueOnFirstIteration(node *head, int iteration)
{
	if(head->val == -1 && iteration == 0)
		return true;
	return false;
}

/* Returns true if the bit is ON (equals to 1). */
bool currentBitIsON(unsigned char currentByte)
{
	if(currentByte%2 == 1)
		return true;
	return false;
}

/* Resets a given set. */
void resetSet(Set *set)
{	
	int i;

	for(i = 0; i < NUM_OF_BYTS_IN_SET; i++)
		*(set->nums+i) = '\0';
}

/* Adds a number to a given set's number list. */
void addNumToSet(Set *set, int num){
	int byteCorresponsingToNum, bitToTurnOn;
	char newBit = 1;
	
	/* [0, 127] / 16 */
	byteCorresponsingToNum = num / BYTE_SIZE;
	 /* [0, 127] % 8 */
	bitToTurnOn = num % BYTE_SIZE;

	/* moves ON bit left num'th times  */
	while(bitToTurnOn > 0){
		newBit = newBit << 1; 
		bitToTurnOn--;
	}
	
	/* adds the new bit to the corresponding byte. */
	*(set->nums+byteCorresponsingToNum) = *(set->nums+byteCorresponsingToNum) | newBit;
}

/* Prints the numbers the given set contains. */
void printSet(Set *set)
{
	int currentBit = 0;
	unsigned char currentByte;
	bool isSetEmpty = true;

	while(currentBit < 128)
	{
		if(currentBit%BYTE_SIZE == 0)
			currentByte = *(set->nums+(currentBit/BYTE_SIZE));

		if(currentBitIsON(currentByte)){
			printf("%d\t", currentBit);
			isSetEmpty = false;
		}

		currentByte = (currentByte >> 1);
		currentBit++;
	}
	printf("\n");

	if(isSetEmpty)
		printf("The set is empty.\n");
}

/* Places the numbers stored in the list that starts with the node head into a given set. */
void readSet(Set *set, node *head)
{
	int iteration = 0;
	node *current;

	current = head;
    	while (current != NULL) {
		if(headIsTerminatingValueOnFirstIteration(head, iteration))
			resetSet(set);	
		else
			if(current->val != -1)
				addNumToSet(set, current->val);

		if(iteration%15 == 0)
			printf("\n");

       		current = current->next;
		iteration++;
        }
}

/* Unites the numbers from the 1st set and the 2nd set and puts them in the 3rd set. */
void unionSet(Set *firstSet, Set *secondSet, Set *thirdSet)
{
	int i;		
	for(i = 0; i < 16; i++)
		*(thirdSet->nums+i) = *(firstSet->nums+i) | *(secondSet->nums+i);
}

/* Subtracts the numbers in the 2nd set from the numbers in the 1st set and put them in the 3rd set. */
void subSet(Set *firstSet, Set *secondSet, Set *thirdSet)
{
	int i;		
	for(i = 0; i < 16; i++)
		*(thirdSet->nums+i) = *(firstSet->nums+i) & (~*(secondSet->nums+i));
}

/* Places the intersection of the 1st and 2nd sets in the 3rd set. */
void intersectSet(Set *firstSet, Set *secondSet, Set *thirdSet)
{
	int i;		
	for(i = 0; i < 16; i++)
		*(thirdSet->nums+i) = *(firstSet->nums+i) & *(secondSet->nums+i);
}

/* Places the symmetric difference of the 1st and 2nd sets in the 3rd set. */
void symDiff(Set *firstSet, Set *secondSet, Set *thirdSet)
{
	int i;		
	for(i = 0; i < 16; i++)
		*(thirdSet->nums+i) = *(firstSet->nums+i) ^ *(secondSet->nums+i);
}

/* Exiting the program */
void stopCommand(char *stopCommandIssued)
{
	*stopCommandIssued = '1';
	printf("Exiting program and releasing memory...\n");
}



