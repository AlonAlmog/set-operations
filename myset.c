#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>
#include "set.h"
#include "errorhandling.h"

void initSets();
void freeSets();
void push(node *head, int val);
void freeList(node *head);
void printFailureAndExit();
bool lineIsEmpty(char *line);
char *allocateMemAndGenerateCopy(char *line);
char *modifyLine(char *line);
char *getCommand(char *line);
void analyzeModLine(char *line, char *stopCommandIssued);
Set *getSet(char *setName);
Set *getFirstSet(char *line);
Set *getSecondSet(char *line);
Set *getThirdSet(char *line);
void getNumList(char *line, node *head);
void sendSetsToCorrespondingCommand(Set *firstSet, Set *secondSet, Set *thirdSet, char *command);

/* Author: Alon Almog
   Finish date: 01/01/2023

   The program serves as a pocket calcualtor that stores numbers in the sets SETA, SETB, ... , SETF.
   Operators that can be used on the sets are print_set, read_set, sub_set, intersect_set, symdiff_set, union_set, stop. 
   Sets can only store numbers between 0 and 127. */

Set *SETA, *SETB, *SETC, *SETD, *SETE, *SETF;

/* The main function gets input (command line) from the user, modifies it so that there's a whitespace before and after each word and comma.
   The main fuction then sends the modified input to be analyzed, which executes the command given there are no errors (in which case the input is skipped). */
int main()
{
	char *line, *modifiedLine, c, stopCommandIssued = '0';
	int lineIndex = 0;
	bool newLine = true;
	initSets();
	
	line = (char*)calloc(MAX_INPUT_SIZE, sizeof(char));
	if(line == NULL)
		printFailureAndExit();

	while((c = getchar()) != EOF)
	{
		if(newLine)
		{	
			printf("\nPlease enter a command:\n- print_set set_name.\n- read_set set_name, num1, num2, ... ,  (list can only store numbers [0, 127] - value to finish list is -1.)\n");
			printf("- Three set commands, such as sub_set, intersect, union_set and symdiff_set, are all formatted as such:\n\tcommand_name input_1st_set_name, input_2nd_set_name, result_set_name\n");
			printf("- Stop command, 'stop'.\n\n");
			newLine = false;
		}
		if(c == '\n' || c == '\0')
		{
			*(line + lineIndex) = '\0';
			modifiedLine = modifyLine(line);
			analyzeModLine(modifiedLine, &stopCommandIssued);
			lineIndex = 0;
			newLine = true;
		} 
		else
		{
			*(line + lineIndex) = c;
			lineIndex++;
		}

		if(stopCommandIssued == '1')
			break;
	}
	
	if(stopCommandIssued == '0')
		printf("Program ternimated incorrectly.\nReleaseing allocared memory and exiting...\n");

	free(line);
	free(modifiedLine);
	freeSets();

	return 0;
}

/* Initializes the sets. */
void initSets()
{
	SETA = (Set*)malloc(sizeof(Set));
	SETB = (Set*)malloc(sizeof(Set));
	SETC = (Set*)malloc(sizeof(Set));
	SETD = (Set*)malloc(sizeof(Set));
	SETE = (Set*)malloc(sizeof(Set));
	SETF = (Set*)malloc(sizeof(Set));

	if(SETA == NULL || SETB == NULL || SETC == NULL || SETD == NULL || SETE == NULL || SETF == NULL)
		printFailureAndExit();

	SETA->nums = (char*)calloc(CHARACTERS_FOR_NUMBER_STORAGE, sizeof(char));
	SETB->nums = (char*)calloc(CHARACTERS_FOR_NUMBER_STORAGE, sizeof(char));
	SETC->nums = (char*)calloc(CHARACTERS_FOR_NUMBER_STORAGE, sizeof(char));
	SETD->nums = (char*)calloc(CHARACTERS_FOR_NUMBER_STORAGE, sizeof(char));
	SETE->nums = (char*)calloc(CHARACTERS_FOR_NUMBER_STORAGE, sizeof(char));
	SETF->nums = (char*)calloc(CHARACTERS_FOR_NUMBER_STORAGE, sizeof(char));
	if(SETA->nums == NULL || SETB->nums == NULL || SETC->nums == NULL || SETD->nums == NULL || SETE->nums == NULL || SETF->nums == NULL)
		printFailureAndExit();	
}

/* Deallocates sets from memory. */
void freeSets()
{
	free(SETA->nums);
	free(SETB->nums);
	free(SETC->nums);
	free(SETD->nums);
	free(SETE->nums);
	free(SETF->nums);

	free(SETA);
	free(SETB);
	free(SETC);
	free(SETD);
	free(SETE);
	free(SETF);
					
}

/* Pushes a given integer value into a given list. */
void push(node *head, int val) 
{
    node *current = head;
    while (current->next != NULL) {
        current = current->next;
    }

    current->next = (node*)malloc(sizeof(node));
    current->next->val = val;
    current->next->next = NULL;
}

/* Frees the set number list. */
void freeList(node *head)
{
	node *tmp;

	while(head != NULL)
	{
		tmp = head;
		head = head->next;
		free(tmp);
	}
}

/* Frees the memory of the altered string (which is a copy of the original one) and points to a copy of the original string. */
void returnToOriginalString(char *originalLine, char *alteredLine)
{
	free(alteredLine);
	alteredLine = allocateMemAndGenerateCopy(originalLine);
}

/* Returns true if line is empty, otherwise returns false. */
bool lineIsEmpty(char *line)
{
	if(*(line) == '\0')
		return true;
	return false;
}

/* Returns a copy of line */
char *allocateMemAndGenerateCopy(char *line)
{
	char *lineCopy;

	lineCopy = (char*)calloc(MAX_INPUT_SIZE, sizeof(char));
	if(lineCopy == NULL)
		printFailureAndExit();

	strcpy(lineCopy, line);

	return lineCopy;
}

/* Modifies it so that there's a whitespace before and after each word and comma. */
char *modifyLine(char *line)
{
	int lineIndex, modifiedLineIndex;
	char currentChar, *modifiedLine;	

	modifiedLine = (char*)calloc(MAX_INPUT_SIZE, sizeof(char));
	if(modifiedLine == NULL)
		printFailureAndExit();

	for(lineIndex = 0, modifiedLineIndex = 0; lineIndex < strlen(line); lineIndex++, modifiedLineIndex++)
	{	
		currentChar = *(line + lineIndex);

		if((currentChar == ',') && (lineIndex != 0))
		{
			if( *(line + lineIndex - 1) != ' ' )
			{
				*(modifiedLine + modifiedLineIndex) = ' ';
				modifiedLineIndex++;
			}

			*(modifiedLine + modifiedLineIndex) = currentChar;
			
			if( *(line + lineIndex + 1) != ' ' )
			{	
				modifiedLineIndex++;
				*(modifiedLine + modifiedLineIndex) = ' ';
			}
		} 
		else 
		{
			if(currentChar == '\t')
				*(modifiedLine + modifiedLineIndex) = ' ';
			else
				*(modifiedLine + modifiedLineIndex) = currentChar;
		}
	}	
		
	return modifiedLine;
}

/* If the first word in the input line is a valid command, returns the command as a string, or NULL otherwise.*/
char *getCommand(char *line)
{
	int commandIndex;
	char *word, *commands[7] = { PRINT_SET, READ_SET, UNION_SET, SUB_SET, INTERSECT_SET, SYMDIFF_SET, STOP };

	word = strtok(line, " ");	
	for(commandIndex = 0; commandIndex < NUM_OF_COMMANDS; commandIndex++)
		if(!lineIsEmpty(line) && !strcmp(word ,commands[commandIndex]))
			return commands[commandIndex];
	
	printf("Undefined command name.\n");
	return NULL;
}

/* Analyzes the modified input command line to make sure there are no errors in it.
   If no errors are found, executes the command. Otherwise prints an error message and skips command. */
void analyzeModLine(char *line, char *stopCommandIssued)
{
	char *command, *lineCopy;
	Set *firstSet, *secondSet, *thirdSet;
	node *head;

	head = (node*)calloc(1, sizeof(node));
	if(head == NULL)
		printFailureAndExit();
	head->val = -1;
	head->next = NULL;

	lineCopy = allocateMemAndGenerateCopy(line);
	if(foundConsecutiveCommas(lineCopy))
		return;

	returnToOriginalString(line, lineCopy);
	command = getCommand(lineCopy);

	returnToOriginalString(line, lineCopy);
	if(command != NULL)
	{
		if(!strcmp(command, PRINT_SET) && !errorsFoundInPrintCommand(lineCopy))
		{
			returnToOriginalString(line, lineCopy);
			firstSet = getFirstSet(lineCopy);
			printSet(firstSet);
		}
		else if(!strcmp(command, READ_SET) && !errorsFoundInReadCommand(lineCopy))
		{
			returnToOriginalString(line, lineCopy);
			firstSet = getFirstSet(lineCopy);
			getNumList(line, head);
			readSet(firstSet, head);
			freeList(head);
		}
		else if(!strcmp(command, STOP) && !errorsFoundInStopCommand(lineCopy))
		{
			free(lineCopy);
			stopCommand(stopCommandIssued);
		}
		else if((!strcmp(command, SUB_SET) || !strcmp(command, UNION_SET) || !strcmp(command, INTERSECT_SET) || !strcmp(command, SYMDIFF_SET)) && !errorsFoundInThreeSetCommand(lineCopy))
		{	
			returnToOriginalString(line, lineCopy);
			firstSet = getFirstSet(lineCopy);

			returnToOriginalString(line, lineCopy);
			secondSet = getSecondSet(lineCopy);

			returnToOriginalString(line, lineCopy);
			thirdSet = getThirdSet(lineCopy);

			sendSetsToCorrespondingCommand(firstSet, secondSet, thirdSet, command);
		}
	} 
	
}

/* Returns the name of the first set. */
Set *getFirstSet(char *line)
{
	char *word;

	word = strtok(line, " "); /* read_set */
	word = strtok(NULL, " "); /* SETA, SETB, ... , SETF */

	return getSet(word);
}

/* Returns the name of the second set. */
Set *getSecondSet(char *line)
{
	char *word;
	
	word = strtok(line, " "); /* read_set */
	word = strtok(NULL, " "); /* SETA, SETB, ... , SETF */
	word = strtok(NULL, " "); /* comma */
	word = strtok(NULL, " "); /* SETA, SETB, ... , SETF */

	return getSet(word);
}

/* Returns the name of the third set. */
Set *getThirdSet(char *line)
{
	char *word;

	word = strtok(line, " "); /* read_set */
	word = strtok(NULL, " "); /* SETA, SETB, ... , SETF */
	word = strtok(NULL, " "); /* comma */
	word = strtok(NULL, " "); /* SETA, SETB, ... , SETF */
	word = strtok(NULL, " "); /* comma */
	word = strtok(NULL, " "); /* SETA, SETB, ... , SETF */

	return getSet(word);
}
 
/* Returns a pointer to the corresponding set (to setName). */
Set *getSet(char *setName)
{
	if(!strcmp(setName, "SETA"))
		return SETA;
	else if(!strcmp(setName, "SETB"))
		return SETB;
	else if(!strcmp(setName, "SETC"))
		return SETC;
	else if(!strcmp(setName, "SETD"))
		return SETD;
	else if(!strcmp(setName, "SETE"))
		return SETE;
	return SETF;
}

/* Gets the numbers list for read_set command. List starts with head pointer. */
void getNumList(char *line, node *head)
{
	char *word;

	word = strtok(line, " "); /* read_set */
	word = strtok(NULL, " "); /* SETA, SETB, ... , SETF */
	word = strtok(NULL, " "); /* comma */
	word = strtok(NULL, " "); /* 1st number */
 
	while(word != NULL)
	{
		if(head->val == -1)
			head->val = atoi(word);
		else
			push(head, atoi(word));

		word = strtok(NULL, " ");
		word = strtok(NULL, " ");
	}	
}

/* Sends the three set parameters to the corresponding command - according to the set name. */
void sendSetsToCorrespondingCommand(Set *firstSet, Set *secondSet, Set *thirdSet, char *command)
{
	if(!strcmp(command, SUB_SET))
		subSet(firstSet, secondSet, thirdSet);
	else if(!strcmp(command, UNION_SET))
		unionSet(firstSet, secondSet, thirdSet);
	else if(!strcmp(command, INTERSECT_SET))
		intersectSet(firstSet, secondSet, thirdSet);
	else 
		symDiff(firstSet, secondSet, thirdSet);
}
















