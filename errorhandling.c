#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>
#include "set.h"
#include "errorhandling.h"

/* Prints message of failed memory allocation and exits the program. */
void printFailureAndExit()
{
	printf("Failed to allocate memory for next command...\nExiting...\n");
	exit(1);
}

/* Returns true if string is integer, otherwise returns false. */
bool isInteger(char *line)
{
	int lineIndex = 0;
	bool firstIteration = true;	

	while(*(line + lineIndex) != '\0')
	{
		if(firstIteration)
		{
			if((*(line + lineIndex) < 48 || *(line + lineIndex) > 57) && *(line + lineIndex) != '-')
			{
				printf("Invalid set member - not an integer.\n");
				return false;
			}
		}
		else 
		{
			if(*(line + lineIndex) < 48 || *(line + lineIndex) > 57)
			{
				printf("Invalid set member - not an integer.\n");
				return false;
			}
		}
		firstIteration = false;
		lineIndex++;
	}

	return true;
}

/* If the given number is between 0 to 127, return true. Otherwise returns false. */
bool integerIsInRange(int num)
{
	if(num >= 0 && num <= 127)
		return true;

	printf("Invalid set memeber - value out of range.\n");
	return false;
}

/* Returns true if word is a valid set, otherwise returns false. */
bool setValid(char *word)
{
	int setIndex;
	char *possibleSets[6] = {"SETA", "SETB", "SETC", "SETD", "SETE", "SETF"};

	for(setIndex = 0; setIndex < 6; setIndex++)
		if(!strcmp(possibleSets[setIndex], word))
			return true;

	printf("Undefined set name.\n");
	return false;
}

/* Returns true if the given string is "-1" - which is terminating value. Returns false otherwise. */
bool inputIsTerminatingValue(char *word)
{
	if(!strcmp(word, "-1"))
		return true;
	return false;
}

/* Returns true if parameters number are too few for corresponding command name. Returns false otherwise. */
bool missingParametersInThreeSetCommand(int wordNumberInLine, char *word)
{
	if(word == NULL && wordNumberInLine < WORDS_IN_THREE_SET_COMMAND + 1)
	{
		printf("Missing paremeter.\n");
		return true;
	}
	return false;
}

/* Returns true if extraneous text has been found in a three set command line. Returns false otherwise. */
bool extraneousTextFoundInThreeSetCommand(int wordNumberInLine, char *word)
{
	if(wordNumberInLine >= WORDS_IN_THREE_SET_COMMAND + 1)
	{
		printf("Extraneous text after end of command.\n");
		return true;
	}
	return false;
}

/* Returns true if extraneous text has been found in stop command line. Returns false otherwise. */
bool extraneousTextFoundInStopCommand(char *word)
{
	if(word != NULL)
	{
		printf("Extraneous text after end of command.\n");
		return true;
	}
	return false;
}

/* Returns true if extraneous text has been found in read_set command line. Returns false otherwise. */
bool extraneousTextFoundAfterInputTerminatingValue(char *word)
{
	if(word != NULL)
	{
		printf("Extraneous text after end of command.\n");
		return true;
	}
	return false;
}

/* Returns true if the given word index is in number position. Returns false otherwise. */
bool wordIsInNumberPosition(int wordIndexInLine)
{
	if(wordIndexInLine%2 == 1)
		return true;
	return false;
}	

/* Returns true of a command is missing a comma. Returns false otherwise. */
bool missingCommaInCommaPosition(int wordIndexInLine, char *word)
{
	if(wordIndexInLine%2 == 0 && strcmp(word, ","))
	{
		printf("Missing comma.\n");
		return true;
	}
	return false;
}

/* Returns true if word is comma. Returns false otherwise. */
bool illegalComma(char *word)
{
	if(!strcmp(word, ","))
	{
		printf("Illegal comma.\n");
		return true;
	}
	return false;
}

/* Returns true if number input list has not ended with terminating value (-1). Returns false otherwise.*/
bool inputHasNotEndedWithTerminatingValue(bool inputTerminateValueRecieved, char *word)
{
	if(!inputTerminateValueRecieved && word == NULL)
	{
		printf("List of set members is not terminated correctly.\n");
		return true;
	}
	return false;
}

/* Returns true if the passed string parameter is a comma. Returns false otherwise. */
bool wordIsComma(char *word)
{
	if(!strcmp(word, ","))
		return true;
	printf("Missing comma.\n");
	return false;
}

/* Return true if 2 or more consecutive commas have been found. Returns false otherwise. */
bool foundConsecutiveCommas(char *line)
{
	char *word, *prevWord = NULL;

	word = strtok(line, " ");
	while(word != NULL)
	{
		if(prevWord != NULL)
		{
			if(!strcmp(word, ",") && !strcmp(prevWord, ","))	
			{
				printf("Multiple consecutive commas.\n");
				return true;
			}
		}
		prevWord = word;
		word = strtok(NULL, " ");
	}

	return false;
}

/* Returns true if errors were found in stop command. Returns false otherwise. */
bool errorsFoundInStopCommand(char *line)
{
	char *word;

	word = strtok(line, " "); /* command name */
	word = strtok(NULL, " "); /* potential 2nd word in line */

	if(extraneousTextFoundInStopCommand(word))
		return true;

	return false;
}

/* Returns true if errors were found in print command. Returns false otherwise. */
bool errorsFoundInPrintCommand(char *line)
{
	char *word;

	word = strtok(line, " "); /* command name */
	word = strtok(NULL, " "); /* set name */

	if(illegalComma(word) || word == NULL)
		return true;
	if(!setValid(word))
		return true;
	
	word = strtok(NULL, " "); /* potential extraneous text */ 
	if(extraneousTextFoundAfterInputTerminatingValue(word))
		return true;

	return false;
}

/* Returns true if errors were found in read command. Returns false otherwise. */
bool errorsFoundInReadCommand(char *line)
{
	char *word;
	int wordIndexInLine = 0;
	bool inputTerminateValueRecieved = false;

	word = strtok(line, " "); /* command name */
	word = strtok(NULL, " "); /* set name */

	if(illegalComma(word) || !setValid(word) || word == NULL)
		return true;

	word = strtok(NULL, " "); /* number list starting with a comma */
	for(wordIndexInLine = 2; word != NULL; wordIndexInLine++)
	{
		if(missingCommaInCommaPosition(wordIndexInLine, word))
			return true;
		if(wordIsInNumberPosition(wordIndexInLine))
		{

			if(isInteger(word))
			{
				if(inputIsTerminatingValue(word))
				{
					inputTerminateValueRecieved = true;
					word = strtok(NULL, " ");
					if(extraneousTextFoundAfterInputTerminatingValue(word))
						return true;
				} 
				else if(!integerIsInRange(atoi(word)))
					return true;	
			}
			else 
				return true;
		}
		if(inputTerminateValueRecieved == false)
		{
			word = strtok(NULL, " ");
			if(inputHasNotEndedWithTerminatingValue(inputTerminateValueRecieved, word))
				return true;		
		}
	}

	return false;
}

/* Returns true if errors were found in a three set command. Returns false otherwise. */
bool errorsFoundInThreeSetCommand(char *line)
{
	char *word;
	int wordNumberInLine;

	word = strtok(line, " ");
	word = strtok(NULL, " "); 

	if(illegalComma(word) || word == NULL)
		return true;

	wordNumberInLine = 2;
	while(word != NULL)
	{	
		if(extraneousTextFoundInThreeSetCommand(wordNumberInLine, word))
			return true;
		else if(wordNumberInLine%2 == 0 && !setValid(word))
			return true;
		else if(wordNumberInLine%2 == 1 && !wordIsComma(word))
			return true;

		word = strtok(NULL, " ");
		wordNumberInLine++;
	}

	if(missingParametersInThreeSetCommand(wordNumberInLine, word))
		return true;

	return false;
}





