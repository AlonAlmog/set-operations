myset: myset.o set.o errorhandling.o
	gcc -ansi -g -Wall -pedantic myset.o set.o errorhandling.o -o myset
myset.o: myset.c set.h errorhandling.h
	gcc -c -ansi -Wall -pedantic myset.c -o myset.o
errorhandling.o: errorhandling.c errorhandling.h
	gcc -c -ansi -Wall -pedantic errorhandling.c -o errorhandling.o
set.o: set.c set.h
	gcc -c -ansi -Wall -pedantic set.c -o set.o
clean:
	rm *.o 
