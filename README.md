# Set Operations

In this program 6 sets are firstly initialized: SETA, SETB, ... , SETF.
Each set can store numbers in range [0, 127], with storage requirements of 1 bit per number.

Storing numbersin a set is done by using the "read_set" command.
Printing numbers stored in a set is done by using the "print_set" command.
Available operands are union, intersect, subtract, symetric difference.
Stoping the program properly is done by using the "stop" command.


