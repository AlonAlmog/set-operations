#define CHARACTERS_FOR_NUMBER_STORAGE 16
#define BYTE_SIZE 8
#define MAX_INPUT_SIZE 200
#define SET_NUMS_INPUT_DONE -1
#define NUM_OF_SETS 6
#define NUM_OF_BYTS_IN_SET 16
#define NUM_OF_COMMANDS 7
#define PRINT_SET "print_set"
#define READ_SET "read_set"
#define UNION_SET "union_set"
#define SUB_SET "sub_set"
#define INTERSECT_SET "intersect_set"
#define SYMDIFF_SET "symdiff_set"
#define STOP "stop"

typedef struct Set{
	char *nums;
} Set;

typedef struct node {
    int val;
    struct node * next;
} node;

bool currentBitIsON(unsigned char currentByte);
void addNumToSet(Set *set, int num);
void resetSet(Set *set);
bool headIsTerminatingValueOnFirstIteration(node *head, int iteration);
void printSet(Set *set);
void readSet(Set *set, node *head);
void unionSet(Set *firstSet, Set *secondSet, Set *thirdSet);
void subSet(Set *firstSet, Set *secondSet, Set *thirdSet);
void intersectSet(Set *firstSet, Set *secondSet, Set *thirdSet);
void symDiff(Set *firstSet, Set *secondSet, Set *thirdSet);
void stopCommand();
